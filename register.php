<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./a.css">
</head>


<body id="body">
    <form action="" method="post">

        <div class="container">
            <fieldset class="group_name">
                <label id="name" for="uname">Họ và tên</label>
                <input id="name_input" type="text" name="uname" required>
            </fieldset>

            <?php
                    $gender = array(0=>"Nam", 1=> "Nữ");
                    $department = array("nothing"=>"Chọn phân khoa", "MAT"=>"Khoa học máy tính",
                    "KDL" => "Khoa học vật liệu");
            ?>

            <fieldset class="group_gender">
                <label id="gender" for="gen">Giới tính</label>
                <?php
                    // foreach($gender as $key => $value):
                    // echo '<option value="'.$key.'">'.$value.'</option>'; //close your tags!!
                    // endforeach;
                    for ($x = 0; $x < 2; $x++) {
                        echo '<input type="radio" name="gender" value="'.$x.'"> '.$gender[$x];
                    }
                ?>
            </fieldset>

            <fieldset class="group_department">
                <label id="department" for="depart">Phân Khoa</label>
                <select id = "select_depart" name="depart">
                <?php 
                    foreach($department as $key => $value):
                        if ($key == "nothing"):
                            echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
                            continue;
                        else:
                            echo $key;
                            echo '<option value="'.$key.'">'.$value.'</option>'; //close your tags!!
                        endif;
                    endforeach;
                ?>
                </select>
            </fieldset>

            <!-- <fieldset class="group_pass">
                <label id="pass" for="psw">Mật khẩu</label>
                <input id="pass_input" type="password" name="psw" required>
            </fieldset> -->

            <fieldset class="register">
                <button id="my_button" type="submit">Đăng ký</button>

                <!-- <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label> -->
            </fieldset>
        </div>

        <!-- <div class="container" style="background-color:#f1f1f1">
  <button type="button" class="cancelbtn">Cancel</button>
  <span class="psw">Forgot <a href="#">password?</a></span>
</div> -->
    </form>

</body>

</html>